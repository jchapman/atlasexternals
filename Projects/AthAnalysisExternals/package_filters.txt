# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthAnalysisExternals.
#
+ External/BAT
+ External/Eigen
+ External/FastJet
+ External/FastJetContrib
+ External/GPerfTools
+ External/GoogleTest
+ External/HDF5
+ External/Lhapdf
+ External/lwtnn
+ External/CheckerGccPlugins
+ External/CLHEP
- .*
