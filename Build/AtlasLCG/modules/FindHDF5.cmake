# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Locate the HDF5 external package. See
#
#    https://cmake.org/cmake/help/v3.8/module/FindHDF5.html
#
# for further information about the variables set up by the module.
#

# The LCG function(s):
include( LCGFunctions )

# Don't allow CMake's FindHDF5.cmake module to look for a hdf5-config.cmake
# file. (Since that would try to set up imported library targets, and we don't
# generally use imported targets for non-ATLAS projects at the moment.)
set( HDF5_NO_FIND_PACKAGE_CONFIG_FILE TRUE )

# Let the helper macro do most of the work. Note that while in some versions of
# CMake the built-in find-module seems to be setting HDF5_LIBRARY_DIRS, in other
# (newer) versions it doesn't. So to be safe, let's set it up ourselves all the
# time.
lcg_wrap_find_module( HDF5 NO_LIBRARY_DIRS )
